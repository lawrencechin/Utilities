#!/bin/bash

# run termdown with a specified time then run
# terminal-notifier to alert termination of timer
termdown $1 && terminal-notifier \
    -sound Glass \
    -appIcon "$UTILS/Terminal_Notifier/reloj.png" \
    -message "🙊 Get going! 🐒" \
    -tile "Timer Elapsed" \
    -subtitle "🙉 Ding! Ding! Ding! 🙈"
