#!/bin/sh

for package in "$@"; do
    if [ -e "$( which $package )" ]; then
        brew uninstall --ignore-dependencies "$package";
    fi

    brew install -f "$( brew info --json $package | jq '.[0].bottle.stable.files.high_sierra.url' | sed s/\"//g )";
done
