#!/bin/bash

red="\033[0;31m"
green="\033[0;32m"
nc="\033[0m"

echo -e "Select : \n${green}s${nc} for gatekeeper status\n${red}d${nc} to disable gatekeeper \n${green}e${nc} to enable\n${red}q${nc} to quit.\n"  
echo -e "••••••••••••••••••"
while true; do
    read REPLY
    if [[ "$REPLY" = "s" ]]; then
        spctl --status
    elif [[ "$REPLY" = "e" ]]; then
        sudo spctl --master-enable
        break
    elif [[ "$REPLY" = "d" ]]; then
        sudo spctl --master-disable
        break
    elif [[ "$REPLY" = "q" ]]; then
        break;
    else
        echo "Please select a valid option\n"
    fi
done;
