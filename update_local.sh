#!/bin/bash

nc="\033[0m"
cyan="\033[0;36m"
magenta="\033[0;35m"
green="\033[0;32m"
bold="\033[1m"
underline="\033[4m"

if [[ "$OSTYPE" == darwin* ]]; then
    brew update;
    echo -e "${bold}Outdated${nc}"
    echo -e "${underline}Brew${nc} 🍺 :"
    echo -e "${cyan}•-----------------•${nc}"
    brew outdated
    brew cask outdated
    cd $HOME/.config/yarn/global
    echo -e "\n${underline}Yarn${nc} 🎯 :"
    echo -e "${magenta}•-----------------•${nc}"
    yarn outdated
    echo -e "\n${underline}Python${nc} 🐍 :"
    echo -e "${green}•-----------------•${nc}"
    pip3 list --outdated --format=freeze
    cd -
else
    echo -e "Outdated\n"
    cd $HOME/.config/yarn/global
    echo -e "Yarn 🎯 : \n"
    yarn outdated
    echo -e "Python 🐍 : \n"
    pip3 list --outdated --format=freeze
    cd -
fi
