#!/bin/bash

# This was adapted from the TMUX plugin tmux-battery https://github.com/tmux-plugins/tmux-battery
# Get the battery status from pmset
# Print Icon, background colour, percentage and remaining based on charged amount
# Consider making a cross platform version in the future as this only works with macOS

Red="\033[0;31m"
Magenta="\033[0;35m"
Green="\033[0;32m"
Cyan="\033[0;36m"
BATTERY="$( pmset -g batt )"
REMAINING="$( echo $BATTERY | grep -o '[0-9]\{1,2\}:[0-9]\{1,2\}' )"
PERCENTAGE="$( echo $BATTERY | grep -o '[0-9]\{1,3\}%' )"
STATUS="$( echo $BATTERY | grep -oE ';(.+);' | sed -E 's/[ ;]//g' )"
ICON_CHARGED="🔋 "
ICON_CHARGING="⚡️ "
ICON_ATTACHED="🔌 "
ICON_DISCHARGING="⏳ "
CHARGED="${Green}"
HIGH_CHARGE="${Cyan}"
MEDIUM_CHARGE="${Magenta}"
LOW_CHARGE="${Red}"

FINALOUTPUT=""

# Check Status to determine icon
# Check Percentage to determine background colour
# Build up FINALOUTPUT string then print
if [[ $STATUS =~ (charged) ]]; then
    FINALOUTPUT="$ICON_CHARGED"
elif [[ $STATUS =~ (^charging) ]]; then
    FINALOUTPUT="$ICON_CHARGING"
elif [[ $STATUS =~ (^discharging) ]]; then
    FINALOUTPUT="$ICON_DISCHARGING"
elif [[ $STATUS =~ (attached) ]]; then
    FINALOUTPUT="$ICON_ATTACHED"
fi

PERCENT="$( echo $PERCENTAGE | sed 's/%//' )"
if [ $PERCENT -eq 100 ]; then
    FINALOUTPUT="$CHARGED$FINALOUTPUT"
elif [ $PERCENT -le 99 -a $PERCENT -ge 51 ]; then
    FINALOUTPUT="$HIGH_CHARGE$FINALOUTPUT"
elif [ $PERCENT -le 51 -a $PERCENT -ge 16 ]; then
    FINALOUTPUT="$MEDIUM_CHARGE$FINALOUTPUT"
else
    FINALOUTPUT="$LOW_CHARGE$FINALOUTPUT"
fi

FINALOUTPUT="$FINALOUTPUT"

if [ -z "$REMAINING" ]; then
    REMAINING="| Unavailable"
else
    REMAINING="| $REMAINING"
fi

echo -e "Battery Status is : $FINALOUTPUT $PERCENTAGE $REMAINING${NC}"
