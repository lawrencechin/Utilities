#!/bin/sh

# Run top command every 15 minutes to try and work out what is causing the
# fans to spin up when machine is idle, screen off but not asleep

# Crontab settings
# */15 * * * * zsh ~/.config/Sys_Check/sample_usage.sh

# Symlink this folder (Sys_check) into ~/.config in order for crontab to run correctly
# iCloud has a horrible path and I use a environment variable to access it but this doesn't work properly in crontab

# Problem seemed to be caused from a bad install of commandline tools/xcode. Having removed and reinstalled commandline tools everything seems to be back to normal. We try and stop the cron job and see how it goes

TODAY="$( date '+%Y-%m-%d' )"
mkdir -p ~/.config/Sys_Check/"$TODAY"
top -o -pid -l 1 > ~/.config/Sys_Check/"$TODAY"/"$( date '+%H-%M-%S' )"_top_output.txt
